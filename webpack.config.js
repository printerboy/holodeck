const path = require('path');
const HtmlWebPackPlugin = require('html-webpack-plugin');

module.exports = {
   entry: './main.js',
   output: {
      path: path.join(__dirname, '/bundle'),
      filename: 'index_bundle.js'
   },
   devServer: {
      inline: true,
      port: 8001
   },
   module: {
      rules: [
         {
            test: /\.jsx?$/,
            exclude: /node_modules/,
            use: {
         	 loader: "babel-loader"
        	}
         }
      ]
   },
   plugins:[
    new HtmlWebPackPlugin({
      template: "./src/index.html",
      filename: "./index.html"
    }) 
   ]
}
